all: run

run: info

info:
	@echo "Necesita tener instalado docker, Node.js y gcc-c++"
	@echo "Para empezar con la instalacion utilice el comando make install"

install:
	@echo "Instalando paquetes..."
	@npm install ./tech-radar-generator
	@echo "Proceso de instalacion finalizado, ahora puede proceder con el compilado make build"

build:
	@echo "Ejecutando libreria y creando los datos de distribucion..."
	@npx tech-radar-generator ./tech_radar_info.json ./dist
	@echo "Proceso de instalacion finalizado, ahora puede proceder con el comando make docker_build"

docker_build:
	@echo "Creando imagen my-tech-radar"
	docker build -t my-tech-radar .
	@echo "Imagen creada correctamente, ahora puede arrancarla utilizando el comando make docker_run"

docker_run:
	@echo "Arrancando imagen my-tech-radar"
	docker run -d -p8080:8080 --name my-tech-radar my-tech-radar

clear:
	@echo "Eliminado archivos residuales..."
	rm -rf ./dist

.PHONY: all